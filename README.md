# Conception CDA PROJECT
 
## Project description

App to find the closest bars as well as the most suitable offers according to the customer's request.


## Project Features

- Users : super admin (me), admin (bar) & users (clients).
- CRUD for the account and for the products offered.
- listing : Access to bar menus as well as all the information of the brands.
- Post/review/photo system for each bar.
- Ability to filter searches based on the different products offered.
- Announcement system for ephemeral events.
- Geolocation system.
- Admin having the ability to filter comments/reviews/image.



## Technos


[Symfony 6] - Framework PHP
[Stimulus JS] - Framework JS
[Flutter] - Framework for mobile app

[Docker] - Container for developing an app
[GitLab] - Team working and versionning



## Deadline

Février 2023

## Class Graphic

[![](https://mermaid.ink/img/pako:eNqlVTFu4zAQ_ArB8hAXaVlclaRJEyCtAIMW1zYRURRWpM9B4AflHflYlpRlkzJlBzmrEXYGs7vDofXBa6uAC143su8ftNygNFXL6LeSyCp-X3G2WBxfOrTKazeLww7aebS2xhAuNULK-TNywot3utG9dOBxVkcbuUkUBFswrdjiL3t5ZjrtH5DWmgDtJNZbiTkmFULfwyweGy0H7QthuV7reguYgcmK6Wb3pc2i-lWHi4wLHbRNRiraWEv39bmxqC-op47hidSQhLH8MdTY4LHILGZHe0XuXih3qPdUz6ihtiRZY6dIbX1Dky5jhwxRsMGvzwJwOpqp1GnPEng-HcEc7BNoPE3BVtY2Q_2Q-hGjfcsNysbQ9ulnNik6oaX1O0DnKTEiFpw2MKGsAQ3MUrbk3S2ZgXNdx9BlCIGbeoPQEdNp27JZexJrb5mUZLNs1hUXHUzPdG7o6FvnV42mSAyjh1Jh9GSeW6N3PXhlSyc5jJBPJnVTvBvU9J9FVcLCXS7vPgb-EilFl53_3SZdstWj6C-ueCYSZv5fjfO1_Z0Sv-OGsi21ok9ZlKi42wKlmwt6VRLfKl61B-L5LuTgUWlnkYu1bHq449I7-_re1lw49DCSjp_DI-vwDd40I18)](https://mermaid.live/edit#pako:eNqlVTFu4zAQ_ArB8hAXaVlclaRJEyCtAIMW1zYRURRWpM9B4AflHflYlpRlkzJlBzmrEXYGs7vDofXBa6uAC143su8ftNygNFXL6LeSyCp-X3G2WBxfOrTKazeLww7aebS2xhAuNULK-TNywot3utG9dOBxVkcbuUkUBFswrdjiL3t5ZjrtH5DWmgDtJNZbiTkmFULfwyweGy0H7QthuV7reguYgcmK6Wb3pc2i-lWHi4wLHbRNRiraWEv39bmxqC-op47hidSQhLH8MdTY4LHILGZHe0XuXih3qPdUz6ihtiRZY6dIbX1Dky5jhwxRsMGvzwJwOpqp1GnPEng-HcEc7BNoPE3BVtY2Q_2Q-hGjfcsNysbQ9ulnNik6oaX1O0DnKTEiFpw2MKGsAQ3MUrbk3S2ZgXNdx9BlCIGbeoPQEdNp27JZexJrb5mUZLNs1hUXHUzPdG7o6FvnV42mSAyjh1Jh9GSeW6N3PXhlSyc5jJBPJnVTvBvU9J9FVcLCXS7vPgb-EilFl53_3SZdstWj6C-ueCYSZv5fjfO1_Z0Sv-OGsi21ok9ZlKi42wKlmwt6VRLfKl61B-L5LuTgUWlnkYu1bHq449I7-_re1lw49DCSjp_DI-vwDd40I18)
